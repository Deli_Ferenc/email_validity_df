#include <stdio.h>

int CheckEmail(char *email)
{
    if(email == NULL)
    {
        printf("Error, E-mail was a nullpointer");
        return 1;
    }

    int i = 0;
    int atSignIndex = -1;
    int dotIndex = -1;

    while(email[i] != 0)
    {
        if(email[i] == '@')
        {
            if(atSignIndex != -1) return 1;
            else atSignIndex = i;
        }
        else if(email[i] == '.')
        {
            if (i == 0) return 1;

            if(email[i-1] == '.' || email[i-1] == '@' ||
                email[i+1] == '.' || email[i+1] == '@') return 1;

            dotIndex = i;
        }
        i++;
    }

    if(atSignIndex != -1 && atSignIndex != 0 &&
        atSignIndex != i-1 && dotIndex != i-1 && dotIndex != -1) return 0;
    else return 1;
}

int main(int argc, char *argv[])
{
    if(argc == 1)
    {
        printf("Give me an e-mail address as an argument!");
        return 1;
    }
    else if(argc > 2)
    {
       printf("Give me only one e-mail address as an argument!");
       return 1;
    }

    if(CheckEmail(argv[1])) printf("This e-mail address is not valid!");
    else printf("This is a valid e-mail address!");

    return 0;
}
